import { makeAutoObservable } from 'mobx';

export default class DeviceStore {
    constructor() {
        this._types = [
            {id: 1, name: 'Fridges'},
            {id: 2, name: 'Phones'}
        ]
        this._brands = [
            {id: 1, name: 'Samsung'},
            {id: 2, name: 'Apple'}
        ]
        this._devices = [
            {
                id: 1,
                name: "12 pro",
                price: 40000,
                rating: 0,
                img: "https://idrop.com.ua/image/catalog/apple/iphone12/iphone12pro/iphone-12-pro-blue-hero.png",
            },
            {
                id: 2,
                name: "12 pro",
                price: 40000,
                rating: 0,
                img: "https://idrop.com.ua/image/catalog/apple/iphone12/iphone12pro/iphone-12-pro-blue-hero.png",
            },
            {
                id: 3,
                name: "12 pro",
                price: 40000,
                rating: 0,
                img: "https://idrop.com.ua/image/catalog/apple/iphone12/iphone12pro/iphone-12-pro-blue-hero.png",
            },
            {
                id: 4,
                name: "12 pro",
                price: 40000,
                rating: 0,
                img: "https://idrop.com.ua/image/catalog/apple/iphone12/iphone12pro/iphone-12-pro-blue-hero.png",
            }
        ]
        makeAutoObservable(this)
    }

    setTypes(types) {
        this._types = types
    }

    setBrands(brands) {
        this._brands = brands
    }

    setDevices(devices) {
        this._devices = devices
    }

    get types() {
        return this._types
    }

    get brands() {
        return this._brands
    }

    get devices() {
        return this._devices
    }
}
