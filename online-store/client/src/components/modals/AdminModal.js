// Models
import { useEffect, useState } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { Form, Field } from 'react-final-form';
import Select from 'react-select';

// Styles
import './styles.scss';

const AdminModal = (props) => {
  const { device, show, onHide, title } = props;
  const [devicesForOption, setDevicesForOption] = useState([]);
  const [brandsForOption, setBrandsForOption] = useState([]);

  const customStyles = {
    control: (base, state) => ({
      ...base,
      background: "white",
      width: '200px',
      borderColor: state.isFocused ? "grey" : "lightgray",
    })
  };

  useEffect(() => {
    if (device) {
      const devs = [];
      const brs = [];

      device.types.map((type) => {
        devs.push({label: type.name, value: type.name})
      })

      device.brands.map((brand) => {
        brs.push({label: brand.name, value: brand.name})
      })

      setDevicesForOption(devs);
      setBrandsForOption(brs);
    }
  }, [device])

  const ReactSelectAdapter = ({ input, ...rest }) => (
    <Select styles={customStyles} {...input} {...rest} searchable />
  )

  const onSubmit = values => {
    return console.log(values);
  }

  return (
    <Modal
      size="lg"
      show={ show }
      onHide={ onHide }
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Add {title}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>

      <Form
        onSubmit={onSubmit}
        render={({ handleSubmit }) => (
        <form onSubmit={handleSubmit}>
          {devicesForOption && title === 'device'
          ? <>
            <Field
            name="devices"
            component={ReactSelectAdapter}
            options={devicesForOption}
            className='mt-3 mb-3'
            />
            <Field
            name="brands"
            component={ReactSelectAdapter}
            options={brandsForOption}
            className='mt-3 mb-3'
            />
          </>
          : <Field 
            name="typeName" 
            component="input" 
            placeholder={`Add ${title} name`}
            style={{width: '100%'}} 
            className='p-2 mt-4'
          /> 
          }
      </form>
    )}
  />

      </Modal.Body>
      <Modal.Footer>
        <Button variant='outline-success' onClick={onHide}>Add</Button>
        <Button variant='outline-danger' onClick={onHide}>Close</Button>
      </Modal.Footer>
    </Modal>
  )
}

export default AdminModal;
