// Modules
import { useContext } from 'react';
import { Context } from '..';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { NavLink } from 'react-router-dom';
import { SHOP_ROUTE } from '../utils/consts';

function NavBar() {
    const { user } = useContext(Context);

    return (
        <>
            <Navbar bg="dark" variant="dark">
                <NavLink style={{ color: 'white', marginLeft: '20px' }} to={SHOP_ROUTE}>Super-Store</NavLink>
                    <Nav className="ml-1">
                        <Nav.Link href="#home">Home</Nav.Link>
                        <Nav.Link href="#features">Features</Nav.Link>
                        <Nav.Link href="#pricing">Pricing</Nav.Link>
                    </Nav>
            </Navbar>
        </>
    );
}

export default NavBar; 
