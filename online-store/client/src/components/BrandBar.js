// Modules
import { useContext } from 'react';
import { Context } from '..';
import { observer } from 'mobx-react-lite';
import { Card, Row } from 'react-bootstrap';

const BrandBar = observer(() => {
  const { device } = useContext(Context);

  return (
    <Row className='d-flex'>
      {device.brands.map(brand =>  
        <Card
        border={brand.id === device.selectedBrand.id ? 'danger' : 'light'}
        className='p-2'
        key={brand.id} 
        onClick={() => device.setSelectedBrand(brand)}
        style={{cursor: 'pointer', width: 'auto', marginRight: '10px'}}
        >
          {brand.name}
        </Card>
      )}
    </Row>
  )
});

export default BrandBar;
