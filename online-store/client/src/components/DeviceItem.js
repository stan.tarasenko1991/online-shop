// Modules
import { Card, Col, Image } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import { DEVICE_ROUTE } from '../utils/consts';

// Assets
import star from '../assets/star.png'

const DeviceItem = (props) => {
  const { device } = props;
  const navigate = useNavigate();

  return (
    <Col md={3} className='mt-3' onClick={() => { navigate(DEVICE_ROUTE + '/' + device.id) }}>
      <Card 
        style={{width: 150, cursor: 'pointer'}}
        border='light'
      >
        <Image 
          src={device.img} 
          height={150}
          width={150} 
        />
        <div className='text-black-50 d-flex justify-content-between align-items-center'>
          <div>Brand</div>
          <div className='d-flex align-items-center'>
            <div>{device.rating}</div>
            <Image src={star} width={15} height={15} />
          </div>
        </div>
        <div>{device.name}</div>
      </Card>
    </Col>
  )
}

export default DeviceItem;
