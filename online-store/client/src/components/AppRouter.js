// Modules
import { useContext } from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
import { Context } from '../index';
import Admin from '../pages/Admin';
import Auth from '../pages/Auth';
import Basket from '../pages/Basket';
import DevicePages from '../pages/DevicePages';
import Shop from '../pages/Shop';
import { ADMIN_ROUTE, BASKET_ROUTE, DEVICE_ROUTE, LOGIN_ROUTE, REGISTRATION_ROUTE, SHOP_ROUTE } from '../utils/consts';

const AppRouter = () => {
const { user } = useContext(Context);

    return (
        <Routes>
            {user.isAuth && 
            <>
                <Route path={ ADMIN_ROUTE } element={<Admin />} />
                <Route path={ BASKET_ROUTE } element={<Basket />} />
            </>
            }

            <Route path={ SHOP_ROUTE } element={<Shop />} exact/>
            <Route path={ DEVICE_ROUTE + '/:id'} element={<DevicePages />} />
            <Route path={ LOGIN_ROUTE } element={<Auth />} />
            <Route path={ REGISTRATION_ROUTE } element={<Auth />} />
            <Route path="*" element={<Navigate to="/" replace />} />
        </Routes>
    )
}

export default AppRouter;
